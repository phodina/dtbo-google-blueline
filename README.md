Minimal dtbo partition for Google Pixel 3 (google-blueline), taken from the AOSP kernel with all device nodes removed.
Used for mainline kernel.
This requires dtc and mkdtboimg (from AOSP libufdt) to build.
